package com.mygdx.screen;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class ResourceManager {
public static AssetManager assets=new AssetManager();
	
	public static void loadAllResources(){
		assets.load("music/Megalovania.mp3",Music.class);
		
		//a�adir m�s elementos
	
	}
	
	public static boolean update(){
		return assets.update();
	}
	
	public static TextureAtlas getAtlas(String path){
		return assets.get(path, TextureAtlas.class);
		
	}
	
	public static Skin getSkin(String path) {
		return assets.get(path,Skin.class);
	}
	
	public static Music getMusic(String path){
		return assets.get(path, Music.class);
	}
	
	public static Sound getSound(String path)
	{
		return assets.get(path, Sound.class);
	}

	public static void dispose(){
		assets.dispose();
	}
}
