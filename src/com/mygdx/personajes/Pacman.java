package com.mygdx.personajes;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Pacman extends BaseActor {
	private Animation animacion;
	private float inicial;
	private int walkAcceleration;
	private float limite;
	private int maxHorizontalSpeed;
	public Pacman(float x, float y, Stage s) {
super(x, y, s);
limite=y-1;
inicial=y+100;
velocityVec.y = 79;
walkAcceleration=20;
maxHorizontalSpeed=50;
		String[] enemy1AnimationFiles = {"assets/pacman/1.png","assets/pacman/2a.png"
				,"assets/pacman/3a.png"};

	    animacion = loadAnimationFromFiles(enemy1AnimationFiles, 0.4f, true);
	}
	@Override
	public void act(float dt) {
		super.act(dt);
		 if (this.getY()>=inicial) {
       	  accelerationVec.add(0 , -walkAcceleration );
		}else if(this.getY()<=limite) {
			 accelerationVec.add( 0, +walkAcceleration );
		}
       velocityVec.add( accelerationVec.x * dt, accelerationVec.y * dt );

       velocityVec.y = MathUtils.clamp( velocityVec.y, 
           -maxHorizontalSpeed, maxHorizontalSpeed );
       moveBy( velocityVec.x * dt, velocityVec.y * dt );
	}

}
