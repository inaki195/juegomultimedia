package com.mygdx.personajes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.graphics.g2d.Animation;

public class Player extends BaseActor {
	  private Animation stand;


		private Animation walk;

	    private float walkAcceleration;
	    private float walkDeceleration;
	    private float maxHorizontalSpeed;
	    private float gravity;
	    private float maxVerticalSpeed;
	    private String inicio;
	    private Animation jump;
	    private float jumpSpeed;
	    private float jumpSpeedChetado=900;
	    private BaseActor belowSensor;
	    boolean invertir;

	    public Player(float x, float y, Stage s)
	    {
	        super(x,y,s);
	        invertir=false;
	        stand = loadTexture( "assets/personaje/personaje0.png" );
	        inicio="assets/personaje/personaje0.png";


	        maxHorizontalSpeed = 100;
	        walkAcceleration   = 400;
	        walkDeceleration   = 400;
	        gravity            = 500;
	        maxVerticalSpeed   = 1000;

	        setBoundaryPolygon(8);

	        jump = loadTexture( cambiarTexture(inicio)  );        
	        jumpSpeed = 450;

	        // set up the below sensor
	        belowSensor = new BaseActor(0,0, s);
	        belowSensor.loadTexture("assets/white.png");
	        belowSensor.setSize( this.getWidth() - 8, 8 );
	        belowSensor.setBoundaryRectangle();
	        belowSensor.setVisible(false);
	    }

	    public String cambiarTexture(String inicio) {
	    	String devuelvo="";
		switch (inicio) {
		case "assets/personaje/personaje0.png":{
			devuelvo="assets/personaje/personaje1.png";
		}
			break;

		case "assets/personaje/personaje1.png":{
			devuelvo="assets/personaje/personaje2.png";
		}
			break;
		case "assets/personaje/personaje2.png":{
			devuelvo="assets/personaje/personaje3.png";
		}
			break;
		case "assets/personaje/personaje3.png":{
			devuelvo="assets/personaje/personaje0.png";
		}	
			break;
		
		}
		return devuelvo;
		}
	    public String cambiarTexture22(String inicio) {
	    	String devuelvo="";
		switch (inicio) {
		case "assets/personaje/personaje0.png":{
			devuelvo="assets/personaje/personaje1.png";
		}
			break;

		case "assets/personaje/personaje1.png":{
			devuelvo="assets/personaje/personaje2.png";
		}
			break;
		case "assets/personaje/personaje2.png":{
			devuelvo="assets/personaje/personaje3.png";
		}
			break;
		case "assets/personaje/personaje3.png":{
			devuelvo="assets/personaje/personaje0.png";
		}	
			break;
		
		}
		return devuelvo;
		}

		public void act(float dt){
	        super.act( dt );

	        // get keyboard input
	        if (!invertir) {
	        	 if (Gdx.input.isKeyPressed(Keys.LEFT))
	 	            accelerationVec.add( -walkAcceleration, 0 );

	 	        if (Gdx.input.isKeyPressed(Keys.RIGHT))
	 	            accelerationVec.add( walkAcceleration, 0 );
			}
	        if (invertir) {
	        	 if (Gdx.input.isKeyPressed(Keys.RIGHT))
	 	            accelerationVec.add( -walkAcceleration, 0 );

	 	        if (Gdx.input.isKeyPressed(Keys.LEFT))
	 	            accelerationVec.add( walkAcceleration, 0 );
			}
	       
	        if (Gdx.input.isKeyPressed(Keys.SPACE))
	        // decelerate when not accelerating
	        if ( !Gdx.input.isKeyPressed(Keys.RIGHT)
	        && !Gdx.input.isKeyPressed(Keys.LEFT)  )
	        {
	            float decelerationAmount = walkDeceleration * dt;

	            float walkDirection;

	            if ( velocityVec.x > 0 )
	                walkDirection = 1;
	            else
	                walkDirection = -1;

	            float walkSpeed = Math.abs( velocityVec.x );

	            walkSpeed -= decelerationAmount;

	            if (walkSpeed < 0)
	                walkSpeed = 0;

	            velocityVec.x = walkSpeed * walkDirection;
	        }
	        

	        // apply gravity
	        accelerationVec.add(0, -gravity);

	        velocityVec.add( accelerationVec.x * dt, accelerationVec.y * dt );

	        velocityVec.x = MathUtils.clamp( velocityVec.x, 
	            -maxHorizontalSpeed, maxHorizontalSpeed );

	        moveBy( velocityVec.x * dt, velocityVec.y * dt );

	        // reset acceleration
	        accelerationVec.set(0,0);

	        // move the below sensor below the koala
	        belowSensor.setPosition( getX() + 4, getY() - 8 );

	        // manage animations
	        if ( this.isOnSolid() )
	        {
	        
	            belowSensor.setColor( Color.GREEN );
	            if ( velocityVec.x == 0 )
	                setAnimation(stand);
	           
	               
	        }
	        else
	        {
	            belowSensor.setColor( Color.RED );
	            setAnimation(jump);
	        }

	        if ( velocityVec.x > 0 ) // face right
	            setScaleX(1);

	        if ( velocityVec.x < 0 ) // face left
	            setScaleX(-1);

	        alignCamera();
	       // boundToWorld();
	    }

	    public boolean belowOverlaps(BaseActor actor)
	    {
	        return belowSensor.overlaps(actor);
	    }

	    public boolean isOnSolid()
	    {
	        for (BaseActor actor : BaseActor.getList( getStage(), "com.mygdx.personajes.Solid" ))
	        {
	        
	            Solid solid = (Solid)actor;
	            if ( belowOverlaps(solid) && solid.isEnebled() )
	                return true;
	        }   

	        return false;
	    }

	    public void jump()
	    {
	        velocityVec.y = jumpSpeed;
	    }

	    public boolean isFalling()
	    {
	        return (velocityVec.y < 0);
	    }

	    public void spring()
	    {
	        velocityVec.y = 1.5f * jumpSpeed;
	    }    

	    public boolean isJumping()
	    {
	        return (velocityVec.y > 0); 
	    }
	    public Animation getStand() {
		return stand;
	}

		public void setStand(Animation stand) {
			cambiarTexture(inicio);
		this.stand = stand;
	}

		public void cambiarimg(int valor) {
		
			if (valor==3) {
				stand = loadTexture( "assets/personaje/personaje0.png" );
				jump=loadTexture( "assets/personaje/personaje0.png" );
			}
			if (valor==1) {
				stand = loadTexture( "assets/personaje/personaje2.png" );
				jump=loadTexture( "assets/personaje/personaje2.png" );
			}
			if (valor==2) {
				stand = loadTexture( "assets/personaje/personaje3.png" );
				jump=loadTexture( "assets/personaje/personaje3.png" );
			}
			if (valor==0) {
				stand = loadTexture( "assets/personaje/personaje1.png" );
				jump=loadTexture( "assets/personaje/personaje1.png" );
			}
			
			
			
		}

		public float getGravity() {
			return gravity;
		}

		public void setGravity(float gravity) {
			this.gravity = gravity;
		}

		public void aplicarGravedad() {
			jumpSpeed=jumpSpeedChetado;
		}

		public void restablecerGravedad() {
			jumpSpeed=450;
			
		}

		public void chetado() {
			maxHorizontalSpeed = 300;
	        walkAcceleration   = 600;
	        walkDeceleration   = 500;
	        gravity            = 450;
	        maxVerticalSpeed   = 1200;
			
		}

		public void valueDefault() {
			maxHorizontalSpeed = 100;
	        walkAcceleration   = 400;
	        walkDeceleration   = 400;
	        gravity            = 500;
	        maxVerticalSpeed   = 1000;
			
		}
		public void invertirvalores(){
			invertir=true;
		}
		public void valuesDefault() {
			invertir=false;
			}
		


	  
	 }


