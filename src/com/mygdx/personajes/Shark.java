package com.mygdx.personajes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Shark extends BaseActor {
	private Animation animacion;
	private BaseActor belowSensor;
	private String[] enemy1AnimationFiles;
	private String[] enemy2AnimationFiles;
	private float walkAcceleration;
	private float maxHorizontalSpeed;
	private float limite;
	private float inicial;
    
	public Shark(float x, float y, Stage s) {
		super(x, y, s);
		limite=x-200;
		inicial=x;
		walkAcceleration=200;
		 enemy1AnimationFiles =new String[] {"assets/shark/0.png", 
				 "assets/shark/1.png",
				 "assets/shark/2.png",
				"assets/shark/3.png",
				"assets/shark/4.png"};
		 enemy2AnimationFiles=new String[]{"assets/shark/dercha/0.png","assets/shark/dercha/1.png",
				 "assets/shark/dercha/2.png","assets/shark/dercha/3.png","assets/shark/dercha/4.png"};
		   animacion = loadAnimationFromFiles(enemy1AnimationFiles, 0.1f, true);
	    	setAnimation(animacion);	
	    	velocityVec.x = 79;
	    	walkAcceleration=20;
	    	maxHorizontalSpeed=50;
	    	belowSensor = new BaseActor(0,0, s);
	        belowSensor.setSize(this.getWidth() - 8, 8 );
	        belowSensor.setBoundaryRectangle();
	        belowSensor.setVisible(false);
	        
	    	
	}
	
	@Override
	public void act(float dt) {
		super.act(dt);
        if (this.getX()>=inicial) {
        	  accelerationVec.add( -walkAcceleration, 0 );
        	cambiarAnimacion(1);
		}else if(this.getX()<=limite) {
			 accelerationVec.add( +walkAcceleration, 0 );
			cambiarAnimacion(2);
		}
        velocityVec.add( accelerationVec.x * dt, accelerationVec.y * dt );

        velocityVec.x = MathUtils.clamp( velocityVec.x, 
            -maxHorizontalSpeed, maxHorizontalSpeed );
        moveBy( velocityVec.x * dt, velocityVec.y * dt );
				
	}

	   public boolean isOnSolid() {
	    	
	        for (BaseActor actor : BaseActor.getList( getStage(), "com.mygdx.personajes.solid" )) {
	            
	        	Solid solid = (Solid)actor;
	            
	            if (overlaps(solid)) {
	                return true;
	            }
	        }   

	        return false;
	    }

	public void setAnimacion(Animation animacion) {
		this.animacion = animacion;
	}
	public void cambiarAnimacion(int r) {

		Animation animacion1= loadAnimationFromFiles(enemy1AnimationFiles, 0.1f, true);
		Animation animacion2=animacion = loadAnimationFromFiles(enemy2AnimationFiles, 0.1f, true);
		if (r==1) {
			setAnimacion ( animacion1 );
		}if(r==2) {
			 setAnimacion(animacion2 );
		}
	}


}
