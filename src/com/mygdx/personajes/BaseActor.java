package com.mygdx.personajes;



import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array; 
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Batch;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.MathUtils;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Intersector.MinimumTranslationVector;

import java.util.ArrayList;
import com.badlogic.gdx.math.Rectangle;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.badlogic.gdx.scenes.scene2d.Group;

public class BaseActor extends Group {

    private Animation<TextureRegion> animation;
    private float elapsedTime;
    private boolean animationPaused;

    public Vector2 velocityVec= new Vector2(0,0);
    protected Vector2 accelerationVec=new Vector2(0,0);
    private float acceleration=0;
    private float maxSpeed=10000;
    private float deceleration=0;

    private Polygon boundaryPolygon;
    private static Rectangle worldBounds;


    public BaseActor() {
		super();
	}
   


	public BaseActor(float x, float y, Stage s) {
    	
        super();

        setPosition(x,y);
        s.addActor(this);

        animation = null;
        elapsedTime = 0;
        animationPaused = false;

        velocityVec = new Vector2(0,0);
        accelerationVec = new Vector2(0,0);
        acceleration = 0;
        maxSpeed = 1000;
        deceleration = 0;

        boundaryPolygon = null;
    }

   

    public void centerAtPosition(float x, float y) {
        
    	setPosition(x - getWidth() / 2 , y - getHeight() / 2);
    }

    public void centerAtActor(BaseActor other) {
        
    	centerAtPosition( other.getX() + other.getWidth()/2 , other.getY() + other.getHeight()/2 );
    }

    
    // M�todos de animaci�n
   

    public void setAnimation(Animation<TextureRegion> anim) {
          animation = anim;
        TextureRegion tr = animation.getKeyFrame(0);
        float w = tr.getRegionWidth();
        float h = tr.getRegionHeight();
        setSize(w, h);
        setOrigin(w / 2, h / 2);
        if (boundaryPolygon == null) {
        	setBoundaryRectangle();
      
        }
    }
    public void Texturizar(String path) {
    	loadTexture(path);
    }
    public static void setWorldBounds(float width, float height)
    {
        worldBounds = new Rectangle( 0,0, width, height );
    }
    public static void setWorldBounds(BaseActor ba)
    {
        setWorldBounds( ba.getWidth(), ba.getHeight() );
    } 
    
    public Animation<TextureRegion> loadAnimationFromFiles(String[] fileNames, float frameDuration, boolean loop) { 
        
    	int fileCount = fileNames.length;
        Array<TextureRegion> textureArray = new Array<TextureRegion>();

        for (int n = 0; n < fileCount; n++) {
        	
            String fileName = fileNames[n];
            Texture texture = new Texture( Gdx.files.internal(fileName) );
            texture.setFilter( TextureFilter.Linear, TextureFilter.Linear );
            textureArray.add( new TextureRegion( texture ) );
        }

        Animation<TextureRegion> anim = new Animation<TextureRegion>(frameDuration, textureArray);

        if (loop) {
        	
            anim.setPlayMode(Animation.PlayMode.LOOP);
        }
        else {
        	
            anim.setPlayMode(Animation.PlayMode.NORMAL);
        }

        if (animation == null) {
        	
            setAnimation(anim);
        }

        return anim;
    }

    
    public Animation<TextureRegion> loadTexture(String fileName) {
    	
        String[] fileNames = new String[1];
        fileNames[0] = fileName;
        return loadAnimationFromFiles(fileNames, 1, true);
    }

    public void setAnimationPaused(boolean pause) {
    	
        animationPaused = pause;
    }

    public boolean isAnimationFinished() {
    	
        return animation.isAnimationFinished(elapsedTime);
    }
    public Animation<TextureRegion> loadAnimationFromSheet(String fileName, int rows, int cols, float frameDuration, boolean loop)
    { 
        Texture texture = new Texture(Gdx.files.internal(fileName), true);
        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        int frameWidth = texture.getWidth() / cols;
        int frameHeight = texture.getHeight() / rows;

        TextureRegion[][] temp = TextureRegion.split(texture, frameWidth, frameHeight);

        Array<TextureRegion> textureArray = new Array<TextureRegion>();

        for (int r = 0; r < rows; r++)
            for (int c = 0; c < cols; c++)
                textureArray.add( temp[r][c] );

        Animation<TextureRegion> anim = new Animation<TextureRegion>(frameDuration, textureArray);

        if (loop)
            anim.setPlayMode(Animation.PlayMode.LOOP);
        else
            anim.setPlayMode(Animation.PlayMode.NORMAL);

        if (animation == null)
            setAnimation(anim);

        return anim;
    }
    
    // M�todos de f�sicas

    public void setAcceleration(float aceleracion) {
    	
        acceleration = aceleracion;
    }

    public void setDeceleration(float deceleracion) {
    	
        deceleration = deceleracion;
    }

    public void setMaxSpeed(float maxSpeed) {
    	
        maxSpeed = maxSpeed;
    }

    public void setSpeed(float speed) {
    	
        if (velocityVec.len() == 0) {
        	
            velocityVec.set(speed, 0);
        }
        else {
        	
            velocityVec.setLength(speed);
        }
    }

    public float getSpeed() {
    	
        return velocityVec.len();
    }

    public boolean isMoving() {
    	
        return (getSpeed() > 0);
    }

    public void setMotionAngle(float angulo) {
    	
        velocityVec.setAngle(angulo);
    }

    public float getMotionAngle() {
    	
        return velocityVec.angle();
    }

    public void accelerateAtAngle(float angulo) {
    	
        accelerationVec.add(new Vector2(acceleration, 0).setAngle(angulo));
    }

    public void accelerateForward() {
    	
        accelerateAtAngle(getRotation());
    }

    public void setBoundaryPolygon(int numSides) {
    	
        float width = getWidth();
        float height = getHeight();

        float[] vertices = new float[2 * numSides];
        
        for (int i = 0; i < numSides; i++) {
        	
            float angle = i * 6.28f / numSides;
            
            vertices[2 * i] = width / 2 * MathUtils.cos(angle) + width/2;
            
            vertices[2 * i + 1] = height / 2 * MathUtils.sin(angle) + height/2;
        }
        
        boundaryPolygon = new Polygon(vertices);

    }

    public Polygon getBoundaryPolygon() {
    	
        boundaryPolygon.setPosition(getX(), getY());
        boundaryPolygon.setOrigin(getOriginX(), getOriginY());
        boundaryPolygon.setRotation( getRotation());
        boundaryPolygon.setScale(getScaleX(), getScaleY()); 
        
        return boundaryPolygon;
    }

    public boolean overlaps(BaseActor other) {
    	
        Polygon poly1 = this.getBoundaryPolygon();
        Polygon poly2 = other.getBoundaryPolygon();

        if (!poly1.getBoundingRectangle().overlaps(poly2.getBoundingRectangle())) {
            
        	return false;
        }

        return Intersector.overlapConvexPolygons( poly1, poly2 );
        
    }
    

    public Vector2 preventOverlap(BaseActor other) {
    	
        Polygon poly1 = this.getBoundaryPolygon();
        Polygon poly2 = other.getBoundaryPolygon();

        if (!poly1.getBoundingRectangle().overlaps(poly2.getBoundingRectangle())) {
        	
            return null;
        }

        MinimumTranslationVector mtv = new MinimumTranslationVector();
        boolean polygonOverlap = Intersector.overlapConvexPolygons(poly1, poly2, mtv);

        if (!polygonOverlap) {
        	
            return null;
        }

        this.moveBy( mtv.normal.x * mtv.depth, mtv.normal.y * mtv.depth );
        
        return mtv.normal;
    }

    public boolean isWithinDistance(float distance, BaseActor other) {
    	
        Polygon poly1 = this.getBoundaryPolygon();
        float scaleX = (this.getWidth() + 2 * distance) / this.getWidth();
        float scaleY = (this.getHeight() + 2 * distance) / this.getHeight();
        poly1.setScale(scaleX, scaleY);
        
        Polygon poly2 = other.getBoundaryPolygon();
        
        if (!poly1.getBoundingRectangle().overlaps(poly2.getBoundingRectangle())) {
        	
            return false;
        }

        return Intersector.overlapConvexPolygons( poly1, poly2 );
    }
  
      

   
    
    
  
    

    public void alignCamera() {
    	
        Camera camera = this.getStage().getCamera();
        Viewport viewport = this.getStage().getViewport();

        camera.position.set( this.getX() + this.getOriginX(), this.getY() + this.getOriginY(), 0 );

   
        camera.update();
    }

    /**
     * Si un borde de un objeto se mueve m�s all� de los l�mites del mundo,
     * Ajuste su posici�n para mantenerlo completamente en la pantalla.
     * */
    public void boundToWorld()
    {
        if (getX() < 0)
            setX(0);
        if (getX() + getWidth() > worldBounds.width)    
            setX(worldBounds.width - getWidth());
        if (getY() < 0)
            setY(0);
        if (getY() + getHeight() > worldBounds.height)
            setY(worldBounds.height - getHeight());
    }

    // Metodos para instanciar

    public static ArrayList<BaseActor> getList(Stage stage, String className) {
    	
        ArrayList<BaseActor> list = new ArrayList<BaseActor>();

        Class clase = null;
        
        try {
        	clase = Class.forName(className);
        }
        catch (Exception error) {
        	error.printStackTrace();
        }

        for (Actor a : stage.getActors()) {
        	
            if ( clase.isInstance( a ) ) {
           
                list.add( (BaseActor)a );
            }
            
        }
       
        return list;
    }

    public static int count(Stage stage, String className) {
        return getList(stage, className).size();
    }

    // M�todos act y draw
 
    public void act(float dt) {
    	
        super.act(dt);

        if (!animationPaused) {
        	
            elapsedTime += dt;
        }
    }
    public void wrapAroundWorld()
    {
        if (getX() + getWidth() < 0)
            setX( worldBounds.width );

        if (getX() > worldBounds.width)    
            setX( -getWidth());

        if (getY() + getHeight() < 0)
            setY( worldBounds.height );

        if (getY() > worldBounds.height)
            setY( -getHeight() );
    }
    
    public void setBoundaryRectangle()
    {
        float w = getWidth();
        float h = getHeight(); 

        float[] vertices = {0,0, w,0, w,h, 0,h};
        boundaryPolygon = new Polygon(vertices);
    }
    public void draw(Batch batch, float parentAlpha) {

        Color color = getColor(); 
        batch.setColor(color.r, color.g, color.b, color.a);

        if (animation != null && isVisible()) {
            batch.draw( animation.getKeyFrame(elapsedTime), getX(), getY(), getOriginX(), getOriginY(),
                getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation() );
        }

        super.draw( batch, parentAlpha );
    }
    
    public void jump() {
		
		velocityVec.y = 400;
		
    }



	public static void setWorldBounds(int mapWidth, int mapHeight) {
		// TODO Auto-generated method stub
		
	}

}