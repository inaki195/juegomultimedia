package com.mygdx.personajes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.model.Animation;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.screen.animation;

public class Protagonista {
Vector2 position;
TextureRegion currentFrame;
float stateTime;
private SpriteBatch batch;
public static final float SPEED=100f;
private Animation animation;
TextureRegion frames[];

public Protagonista(float x,float y){
	position=new Vector2(x,y);
	batch=new SpriteBatch();
	Sprite sprite1=new Sprite(new Texture(Gdx.files.internal("assets/personaje0.png")));
	Sprite sprite2=new Sprite(new Texture(Gdx.files.internal("assets/personaje1.png")));
	Sprite sprite3=new Sprite(new Texture(Gdx.files.internal("assets/personaje2.png")));
	Sprite sprite4=new Sprite(new Texture(Gdx.files.internal("assets/personaje3.png")));
	 frames=new TextureRegion[]{
		sprite1,sprite2,sprite3,sprite4
	};
	//cargar la animacion
	animation=new Animation();
	
}
public void move(Vector2 movement){
movement.scl(SPEED);
position.add(movement);

}
public void render(SpriteBatch batch) {
	batch.draw(currentFrame, position.x, position.y);
	
}
public void update(float dt) {
	stateTime+=dt;
	move(new Vector2(dt,0));

}
}
