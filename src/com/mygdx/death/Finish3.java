package com.mygdx.death;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.mygdx.game.Geometry;
import com.mygdx.menu.BaseScreen;
import com.mygdx.menu.MainMenuGameScreen;
import com.mygdx.niveles.Nivel1;
import com.mygdx.niveles.Nivel2;
import com.mygdx.niveles.Nivel3;
import com.mygdx.personajes.BaseActor;


public class Finish3 extends BaseScreen {
	
	Music song;

	@Override
	public void initialize() {		
		BaseActor gameOver = new BaseActor(0,0, mainStage);
		gameOver.loadTexture("assets/gameover.png");
		gameOver.setSize(800, 640);
	
		
		song = Gdx.audio.newMusic(Gdx.files.internal("assets/music/astronimia.mp3"));
		song.setLooping(false);
		song.setVolume(0.3f);
		song.play();
		
	}

	@Override
	public void update(float dt) {
		
		if (Gdx.input.isKeyPressed(Keys.ENTER)) {
			
			song.pause();
			
			Geometry.setActiveScreen(new Nivel3() );
			
		}
		if (Gdx.input.isKeyPressed(Keys.A)) {
			
			song.pause();
			
			Geometry.setActiveScreen(new MainMenuGameScreen() );
			
		}
		
	}

}
