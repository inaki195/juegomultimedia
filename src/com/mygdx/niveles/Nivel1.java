
package com.mygdx.niveles;
import java.io.File;
import java.util.ArrayList;

import javax.swing.text.Position;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthoCachedTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.mygdx.Tiled.TilemapActor;
import com.mygdx.death.Finish;
import com.mygdx.game.Geometry;
import com.mygdx.menu.BaseGame;
import com.mygdx.menu.BaseScreen;
import com.mygdx.menu.MainMenuGameScreen;
import com.mygdx.personajes.BaseActor;
import com.mygdx.personajes.Destructor;
import com.mygdx.personajes.Fin;
import com.mygdx.personajes.Flag;
import com.mygdx.personajes.Luna;
import com.mygdx.personajes.Pinchos;
import com.mygdx.personajes.Player;
import com.mygdx.personajes.Power;
import com.mygdx.personajes.Pacman;
import com.mygdx.personajes.Shark;
import com.mygdx.personajes.Solid;
import com.mygdx.personajes.Speed;


public class Nivel1 extends BaseScreen{
		private  boolean gameOver;
		private TiledMap map;
	    private OrthographicCamera tiledCamera;
	    private OrthoCachedTiledMapRenderer tiledMapRenderer;
	    TilemapActor tiledMapActor;
	    Music song;
	    Player cubo;
	    Label powerups;
	    Label powerups2;
	    Label empty;
	    MapObject startPoint;
	    MapProperties startProps;
	    int valor=0;
	    Sound deathSound;
	    Label powerup3;
	    Label powerup2;
	    Label powerup1;
	    Label messageLabel;
	    Label timeLabel;
	    Table keyTable;
	    boolean inmunidad;
	    float inmunidadActual;
		float duracion;
		float time;
		

@Override
public void initialize() {
	//cargo el mapa
	time=90;
	inmunidad=false;
	tiledMapActor = new TilemapActor("assets/maps/mapa2.tmx", mainStage);
	duracion=10f;
	inmunidadActual=0;
	//cargo musica de fondo y el sonido de muerte
	deathSound = Gdx.audio.newSound(Gdx.files.internal("assets/sound/roblox-death.mp3"));
	song = Gdx.audio.newMusic(Gdx.files.internal("assets/music/TimeMachine.mp3"));
	song.setLooping(true);
	song.setVolume(0.6f);
	song.play();
	ArrayList<Destructor>uwu=new ArrayList<Destructor>();
	MapObject oj=new MapObject();
	
    for (MapObject obj : tiledMapActor.getRectangleList("Solid") ) {
    	
        MapProperties props = obj.getProperties();
        Solid s;
        s=new Solid( (float)props.get("x"), (float)props.get("y"), (float)props.get("width"),
        		(float)props.get("height"), mainStage );
    
             
    }
     startPoint = tiledMapActor.getRectangleList("inicio").get(0);
 
     startProps= startPoint.getProperties();
     cubo = new Player((float) startProps.get("x"), (float)startProps.get("y"), mainStage);
     
     for (MapObject obj : tiledMapActor.getobjectlist("pacman")) {
         MapProperties props = obj.getProperties();
         new Pacman((float)props.get("x"), (float)props.get("y"), mainStage);
       
         
     }
    
     
     for (MapObject obj : tiledMapActor.getobjectlist("shark")) {
    
         MapProperties props = obj.getProperties();
         
         Shark shark=new Shark((float)props.get("x"), (float)props.get("y"), mainStage);
       
        
     }
   
     for (MapObject obj : tiledMapActor.getobjectlist("luna")) {
    	   
         MapProperties props = obj.getProperties();
         
         new Luna((float)props.get("x"), (float)props.get("y"), mainStage);  
     }
     for (MapObject obj : tiledMapActor.getobjectlist("speed")) {
  	   
         MapProperties props = obj.getProperties();
         
         new Speed((float)props.get("x"), (float)props.get("y"), mainStage);  
     }
     for (MapObject obj : tiledMapActor.getobjectlist("destructor")) {
    	   
         MapProperties props = obj.getProperties();
         
        Destructor d= new Destructor((float)props.get("x"), (float)props.get("y"), mainStage);
        
     }
     
 
   
     for (MapObject obj : tiledMapActor.getTileList("pacman")) {
         MapProperties props = obj.getProperties();
         Pacman pacman=new Pacman((float)props.get("x"), (float)props.get("y"), mainStage);
        
    
     } 
     
     for (MapObject obj : tiledMapActor.getobjectlist("final")) {	 
         MapProperties props = obj.getProperties();
       
         new Flag((float)props.get("x"), (float)props.get("y"), mainStage);
         
     }
     
     powerup1 = new Label("", BaseGame.labelStyle);
     powerup2=new Label("", BaseGame.labelStyle);
     powerup3=new Label("",BaseGame.labelStyle);
     timeLabel = new Label("  Time: " + (int)time, BaseGame.labelStyle);
     timeLabel.setColor(Color.LIGHT_GRAY);
     empty=new Label("      ",BaseGame.labelStyle);
     powerup1.setColor(Color.GREEN);
     powerup2.setColor(Color.GREEN);
     powerup3.setColor(Color.GREEN);

     keyTable = new Table();
   
     messageLabel = new Label("Message", BaseGame.labelStyle);
     messageLabel.setVisible(false);        

     uiTable.pad(20);
     uiTable.add(powerup1);
    // uiTable.add(keyTable).expandX();
     uiTable.add(empty);
     uiTable.add(powerup2);
     uiTable.add(empty);
     uiTable.add(powerup3);
     uiTable.add(timeLabel);
     uiTable.row(); 
     timeLabel.setVisible(true);
     uiTable.add(messageLabel).colspan(3).expandY();
    
     cubo.toFront();
   
     
	
}

@Override
/**
 * control de colisiones
 */
public void update(float dt) {
	for (BaseActor actor : BaseActor.getList(mainStage, "com.mygdx.personajes.Solid"))
    {
        Solid solid = (Solid)actor;
        if ( cubo.overlaps(solid) && solid.isEnebled() )
        {
            Vector2 offset = cubo.preventOverlap(solid);

            if (offset != null)
            {
                // collided in X direction
                if ( Math.abs(offset.x) > Math.abs(offset.y) )
                    cubo.velocityVec.x = 0;
                else // collided in Y direction
                    cubo.velocityVec.y = 0;
            }
        }
    }
	
	//colision con el pacman
	for (BaseActor pacman : BaseActor.getList(mainStage, "com.mygdx.personajes.Pacman"))
    {
		if (cubo.overlaps(pacman)) {
    		if(inmunidad==false) {
    		deathSound.play(0.3f);
    		song.pause();
    		Geometry.setActiveScreen(new Finish());
    		}else {
				
			}
        }
    }
	for (BaseActor shark : BaseActor.getList(mainStage, "com.mygdx.personajes.Shark"))
    {
		if (cubo.overlaps(shark)) {
    		if(inmunidad==false) {
    		deathSound.play(0.3f);
    		song.pause();
    		Geometry.setActiveScreen(new Finish());
    		}else {
				
			}
        }
    }
	 time -= dt;
     timeLabel.setText("Time: " + (int)time);
	for (BaseActor flag : BaseActor.getList(mainStage, "com.mygdx.personajes.Flag"))
    {
		if (cubo.overlaps(flag)) {
			song.stop();
             cubo.remove();
    		Geometry.setActiveScreen(new MainMenuGameScreen());
           
        }
    } 
	//cargamos el power ups lunar
	for (BaseActor luna : BaseActor.getList(mainStage, "com.mygdx.personajes.Luna"))
    {
		//si colisiona
		if (cubo.overlaps(luna)) {
			luna.remove();
			inmunidadActual=duracion;
			
        }
    }
	 if (time <= 0)
     {
      
         messageLabel.setColor(Color.RED);
         messageLabel.setVisible(true);
         cubo.remove();
         Geometry.setActiveScreen(new Finish());
     }
	//comprobacion de los power ups
	 if (inmunidadActual > 0) {
	     	powerup1.setText("INMUNIDAD");
	     	inmunidadActual -= Gdx.graphics.getDeltaTime();
	     	inmunidad=true;
	     	if (inmunidadActual<4) {
				powerup1.setColor(Color.ORANGE);
			}
	     	if (inmunidadActual<2) {
				powerup1.setColor(Color.RED);
			}
	     	
	     } else if (inmunidadActual <= 0) {
	    	 powerup1.setText("");
	     	inmunidad=false;
	     }	
} 
/**
 * metodo que hedera de la baseScreen detecta el teclado y taton
 */
public boolean keyDown(int keyCode)
{
    if (gameOver) {
    
        return false;
    }
   
    if (keyCode == Keys.SPACE)
    {
    	
       
        if ( Gdx.input.isKeyPressed(Keys.DOWN) )
        {
            
        }
        else if ( cubo.isOnSolid() )  
        {
        	String texture="";
			cubo.cambiarimg(valor);
			valor++;
			if(valor>=4) {
				valor=0;
			}
            cubo.jump();
           
        }
    
 
}
    return false;
}



	
}
