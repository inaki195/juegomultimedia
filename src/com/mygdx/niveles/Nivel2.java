package com.mygdx.niveles;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthoCachedTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.mygdx.Tiled.TilemapActor;
import com.mygdx.death.Finish;
import com.mygdx.death.Finish2;
import com.mygdx.game.Geometry;
import com.mygdx.menu.BaseGame;
import com.mygdx.menu.BaseScreen;
import com.mygdx.menu.MainMenuGameScreen;
import com.mygdx.personajes.BaseActor;
import com.mygdx.personajes.Destructor;
import com.mygdx.personajes.Flag;
import com.mygdx.personajes.Luna;
import com.mygdx.personajes.Pacman;
import com.mygdx.personajes.Player;
import com.mygdx.personajes.Shark;
import com.mygdx.personajes.Solid;
import com.mygdx.personajes.Speed;

public class Nivel2 extends BaseScreen {
	
		private  boolean gameOver;
		private TiledMap map;
	    private OrthographicCamera tiledCamera;
	    private OrthoCachedTiledMapRenderer tiledMapRenderer;
	    TilemapActor tiledMapActor;
	    Music song;
	    Player cubo;
	    Label powerups;
	    Label powerups2;
	    Label empty;
	    MapObject startPoint;
	    MapProperties startProps;
	    int valor=0;
	    Sound deathSound;
	    Label powerup3;
	    Label powerup2;
	    Label powerup1;
	    Label messageLabel;
	    Label timeLabel;
	    Table keyTable;
	    boolean inmunidad;
	    boolean desctructor;
	    float inmunidadActual;
		float duracion;
		float destructorActual;
		float duraciondestructora;
		float speedActual;
		float duracionspeed;
		float time;
		

@Override
public void initialize() {
	//cargo el mapa
	time=90;
	inmunidad=false;
	desctructor=false;
	tiledMapActor = new TilemapActor("assets/maps/mapa3.tmx", mainStage);
	//duracion de los power ups
	duraciondestructora=7f;
	duracion=10f;
	duraciondestructora=12f;
	inmunidadActual=0;
	destructorActual=0;
	speedActual=0;
	//cargo musica de fondo y el sonido de muerte
	deathSound = Gdx.audio.newSound(Gdx.files.internal("assets/sound/roblox-death.mp3"));
	song = Gdx.audio.newMusic(Gdx.files.internal("assets/music/BackOnTrack.mp3"));
	song.setLooping(true);
	song.setVolume(0.6f);
	song.play();
	ArrayList<Destructor>uwu=new ArrayList<Destructor>();
	MapObject oj=new MapObject();
	
    for (MapObject obj : tiledMapActor.getRectangleList("Solid") ) {
    	
        MapProperties props = obj.getProperties();
        Solid s;
        s=new Solid( (float)props.get("x"), (float)props.get("y"), (float)props.get("width"),
        		(float)props.get("height"), mainStage );
    
             
    }
     startPoint = tiledMapActor.getRectangleList("inicio").get(0);
 
     startProps= startPoint.getProperties();
     cubo = new Player((float) startProps.get("x"), (float)startProps.get("y"), mainStage);
     
     for (MapObject obj : tiledMapActor.getobjectlist("pacman")) {
         MapProperties props = obj.getProperties();
         new Pacman((float)props.get("x"), (float)props.get("y"), mainStage);
       
         
     }
    
     
     for (MapObject obj : tiledMapActor.getobjectlist("shark")) {
    
         MapProperties props = obj.getProperties();
         
         new Shark((float)props.get("x"), (float)props.get("y"), mainStage);
       
        
     }
   
     for (MapObject obj : tiledMapActor.getobjectlist("luna")) {
    	   
         MapProperties props = obj.getProperties();
         
         new Luna((float)props.get("x"), (float)props.get("y"), mainStage);  
     }
     for (MapObject obj : tiledMapActor.getobjectlist("speed")) {
  	   
         MapProperties props = obj.getProperties();
         
         new Speed((float)props.get("x"), (float)props.get("y"), mainStage);  
     }
     for (MapObject obj : tiledMapActor.getobjectlist("destructor")) {
    	   
         MapProperties props = obj.getProperties();
         
        Destructor d= new Destructor((float)props.get("x"), (float)props.get("y"), mainStage);
        
     }
     System.out.println(uwu.size());
 
   
     for (MapObject obj : tiledMapActor.getTileList("pacman")) {
         MapProperties props = obj.getProperties();
         Pacman pacman=new Pacman((float)props.get("x"), (float)props.get("y"), mainStage);
        
    
     } 
     
     for (MapObject obj : tiledMapActor.getobjectlist("final")) {	 
         MapProperties props = obj.getProperties();
       
         new Flag((float)props.get("x"), (float)props.get("y"), mainStage);
         
     }
     
     powerup1 = new Label("", BaseGame.labelStyle);
     powerup2=new Label("", BaseGame.labelStyle);
     powerup3=new Label("",BaseGame.labelStyle);
     empty=new Label("      ",BaseGame.labelStyle);
     timeLabel = new Label("Time: " + (int)time, BaseGame.labelStyle);
     timeLabel.setColor(Color.LIGHT_GRAY);
     powerup2.setColor(Color.GREEN);
     powerup1.setColor(Color.GREEN);
     powerup3.setColor(Color.GREEN);
     keyTable = new Table();
   
     messageLabel = new Label("Message", BaseGame.labelStyle);
     messageLabel.setVisible(false);        

     uiTable.pad(20);
     uiTable.add(powerup1);
    // uiTable.add(keyTable).expandX();
     uiTable.add(empty);
     uiTable.add(powerup2);
     uiTable.add(empty);
     uiTable.add(powerup3);
     uiTable.add(timeLabel);
     uiTable.row(); 
     uiTable.add(messageLabel).colspan(3).expandY();
    
     cubo.toFront();
	
}

@Override
/**
 * control de colisiones
 */
public void update(float dt) {
	 time -= dt;
     timeLabel.setText("  Time: " + (int)time);
     if (time <= 0)
     {
         messageLabel.setText("Time Up - Game Over");
         messageLabel.setColor(Color.RED);
         messageLabel.setVisible(true);
         cubo.remove();
        Geometry.setActiveScreen(new Finish2());
     }
	for (BaseActor actor : BaseActor.getList(mainStage, "com.mygdx.personajes.Solid"))
    {
        Solid solid = (Solid)actor;
        if ( cubo.overlaps(solid) && solid.isEnebled() )
        {
            Vector2 offset = cubo.preventOverlap(solid);

            if (offset != null)
            {
                // collided in X direction
                if ( Math.abs(offset.x) > Math.abs(offset.y) )
                    cubo.velocityVec.x = 0;
                else // collided in Y direction
                    cubo.velocityVec.y = 0;
            }
        }
    }
	
	//colision con el pacman
	for (BaseActor pacman : BaseActor.getList(mainStage, "com.mygdx.personajes.Pacman"))
    {
		
		if (cubo.overlaps(pacman)) {
    		if(inmunidad==false) {
    			
    		deathSound.play(0.3f);
    		song.pause();
    		Geometry.setActiveScreen(new Finish2());
    		}
    		
        }
    }
	
	for (BaseActor shark : BaseActor.getList(mainStage, "com.mygdx.personajes.Shark"))
    {
		if (cubo.overlaps(shark)) {
			
    		if(inmunidad==false) {
    		deathSound.play(0.3f);
    		song.pause();
    		Geometry.setActiveScreen(new Finish2());
    		}else if(!inmunidad) {
				if(desctructor){
					shark.remove();
				}
			}
        }
		
    }
	for (BaseActor flag : BaseActor.getList(mainStage, "com.mygdx.personajes.Flag"))
    {
		if (cubo.overlaps(flag)) {
			song.stop();
             cubo.remove();
    		Geometry.setActiveScreen(new MainMenuGameScreen());
           
        }
    } 
	//cargamos el power ups lunar
	for (BaseActor luna : BaseActor.getList(mainStage, "com.mygdx.personajes.Luna"))
    {
		//si colisiona
		if (cubo.overlaps(luna)) {
			luna.remove();
			inmunidadActual=duracion;
			
        }
    }
	for (BaseActor destructor : BaseActor.getList(mainStage, "com.mygdx.personajes.Destructor"))
    {
		//si colisiona
		if (cubo.overlaps(destructor)) {
			destructor.remove();
			destructorActual=duraciondestructora;
			
        }
    }
	for (BaseActor speed : BaseActor.getList(mainStage, "com.mygdx.personajes.Speed"))
    {
		//si colisiona
		if (cubo.overlaps(speed)) {
			speed.remove();
			speedActual=duraciondestructora;
			
        }
    }
	//comprobacion de los power ups
	 if (inmunidadActual > 0) {
     	powerup1.setText("INMUNIDAD");
     	inmunidadActual -= Gdx.graphics.getDeltaTime();
     	inmunidad=true;
     	if (inmunidadActual<4) {
			powerup1.setColor(Color.ORANGE);
		}
     	if (inmunidadActual<2) {
			powerup1.setColor(Color.RED);
		}
     	
     } else if (inmunidadActual <= 0) {
    	 powerup1.setText("");
     	inmunidad=false;
     	powerup1.setColor(Color.GREEN);
     	
     }
	 //destructor
	 if (destructorActual > 0) {
	     	powerup2.setText("INVERTIDO");
	     	destructorActual -= Gdx.graphics.getDeltaTime();
	     	cubo.invertirvalores();
	     	if (destructorActual<4) {
				powerup2.setColor(Color.ORANGE);
			}
	     	if (inmunidadActual<2) {
				powerup2.setColor(Color.RED);
			}
	     	
	     } else if (inmunidadActual <= 0) {
	    	 powerup2.setText("");
	     	desctructor=false;
	     	cubo.valuesDefault();
	    	powerup2.setColor(Color.GREEN);
	     }
	 if (speedActual > 0) {
		 	powerup3.setVisible(true);
	     	powerup3.setText("SPEED");
	     	speedActual -= Gdx.graphics.getDeltaTime();
	     	cubo.chetado();
	     	
	     	if (speedActual<4) {
				powerup3.setColor(Color.ORANGE);
			}
	     	if (speedActual<2) {
				powerup3.setColor(Color.RED);
			}
	     	
	     } else if (speedActual <= 0) {
	    	 powerup3.setText("");
	     	cubo.valueDefault();
	     	powerup3.setColor(Color.GREEN);
	     	
	     }
	
	
	
} 
/**
 * metodo que hedera de la baseScreen detecta el teclado y taton
 */
public boolean keyDown(int keyCode)
{
    if (gameOver) {
    
        return false;
    }
   
    if (keyCode == Keys.SPACE)
    {
    	
       
        if ( Gdx.input.isKeyPressed(Keys.DOWN) )
        {
            
        }
        else if ( cubo.isOnSolid() )  
        {
        	String texture="";
			cubo.cambiarimg(valor);
			valor++;
			if(valor>=4) {
				valor=0;
			}
            cubo.jump();
           
        }
    
 
}
    return false;
}
@Override
public void dispose() {

super.dispose();
}



	
}


