package com.mygdx.Tiled;

import java.util.ArrayList;


import java.util.Iterator;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject;
import com.badlogic.gdx.maps.tiled.renderers.OrthoCachedTiledMapRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.mygdx.personajes.BaseActor;
import com.mygdx.personajes.Solid;

public class TilemapActor extends Actor {
	public static int windowWidth  = 900;
    public static int windowHeight = 740;
    private TiledMap tiledMap;
    private OrthographicCamera tiledCamera;
    private OrthoCachedTiledMapRenderer tiledMapRenderer;
    int tileWidth ;
    public TilemapActor(String filename, Stage theStage) {

    	tiledMap = new TmxMapLoader().load(filename);

         tileWidth = (int)tiledMap.getProperties().get("tilewidth");
        int tileHeight = (int)tiledMap.getProperties().get("tileheight");
        int numTilesHorizontal = (int)tiledMap.getProperties().get("width");
        int numTilesVertical = (int)tiledMap.getProperties().get("height");
        int mapWidth = tileWidth  * numTilesHorizontal;
        int mapHeight = tileHeight * numTilesVertical;

        tiledMapRenderer = new OrthoCachedTiledMapRenderer(tiledMap);
        tiledMapRenderer.setBlending(true);
        tiledCamera = new OrthographicCamera();
        tiledCamera = (OrthographicCamera) theStage.getCamera();
        tiledCamera.setToOrtho(false, windowWidth, windowHeight);
        tiledCamera.update();

        theStage.addActor(this);

        BaseActor.setWorldBounds(mapWidth, mapHeight);
        
    }
    /**
     * 
	 *	Busca en las capas del mapa Objetos rectangulares que contengan una propiedad (clave) 
	 *llamada "nombre" con el valor asociado propertyName.
     * Normalmente se usa para almacenar información que no es de actores,
     *  como ubicaciones de SpawnPoint o dimensiones de objetos sólidos.
     * Recuperar datos como objeto, luego convertir al tipo deseado: por ejemplo,
     *  float w = (float) obj.getProperties (). Get ("width").
     * @param propertyName
     * @return
     */
	public ArrayList<MapObject> getRectangleList(String propertyName) {
		  ArrayList<MapObject> list = new ArrayList<MapObject>();
		  	System.out.println(tiledMap.getLayers());
	        for (MapLayer layer : tiledMap.getLayers()) {
	        	//van capa por capa
	            for (MapObject obj : layer.getObjects()) {
	        
	                if (!(obj instanceof RectangleMapObject)) {
	                    continue;
	                }
	            
	                MapProperties props = obj.getProperties();

	                if (props.containsKey("name") && props.get("name").equals(propertyName)) {
	               
	                    list.add(obj);
	                }
	            }
	        }
	     
	        
	        return list;
	}
	public ArrayList<MapObject> getobjectlist(String propertyName) {
		  ArrayList<MapObject> list = new ArrayList<MapObject>();
		  	System.out.println(tiledMap.getLayers());
	        for (MapLayer layer : tiledMap.getLayers()) {
	        	//van capa por capa
	            for (MapObject obj : layer.getObjects()) {
	        
	                MapProperties props = obj.getProperties();
	                
	                if (props.containsKey("name") && props.get("name").equals(propertyName)) {
	               
	                    list.add(obj);
	                }
	            }
	        }
	     
	        
	        return list;
	}
public ArrayList<MapObject> getTileList(String propertyName) {
    	
        ArrayList<MapObject> list = new ArrayList<MapObject>();

        for ( MapLayer layer : tiledMap.getLayers()) {
        	
            for ( MapObject obj : layer.getObjects()) {
            	
                if (!(obj instanceof TiledMapTileMapObject)) {
                    continue;
                }

                MapProperties props = obj.getProperties();

                TiledMapTileMapObject tmtmo = (TiledMapTileMapObject)obj;
                TiledMapTile t = tmtmo.getTile();
                MapProperties defaultProps = t.getProperties();
             
                if ( defaultProps.containsKey("name") && defaultProps.get("name").equals(propertyName)) {
                    list.add(obj);
                   
                }

                Iterator<String> propertyKeys = defaultProps.getKeys();

                while (propertyKeys.hasNext()) {
                	
                    String key = propertyKeys.next();

                    if ( props.containsKey(key) ) {
                        continue;
                    }
                    else {
                        Object value = defaultProps.get(key);
                        props.put( key, value );
                    }
                }
            }
        }
        
        return list;
    }
	@Override
	public void act(float delta) {

		super.act(delta);
	}
	@Override
	public void draw(Batch batch, float parentAlpha) {
		  Camera mainCamera = getStage().getCamera();
		  	tiledCamera.position.x=tiledCamera.position.x+0.9f;
	        tiledCamera.position.y = mainCamera.position.y;
	        tiledCamera.update();
	       
	        tiledMapRenderer.setView(tiledCamera);

	        // need the following code to force batch order,
	        //  otherwise it is batched and rendered last
	        batch.end();
	        tiledMapRenderer.render();        
	        batch.begin();
	}
}
