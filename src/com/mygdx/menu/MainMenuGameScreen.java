package com.mygdx.menu;

import java.util.ArrayList;



import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g3d.model.Animation;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Geometry;
import com.mygdx.niveles.Nivel1;
import com.mygdx.niveles.Nivel2;
import com.mygdx.niveles.Nivel3;
import com.mygdx.personajes.Actorcubo;
import com.mygdx.personajes.BaseActor;

public  class MainMenuGameScreen extends BaseScreen {
	
	private  Texture button;
	Skin skin;
	Skin alternativa;
	private Actorcubo prota;
	Animation animacione;
	ArrayList<Sprite>animacion;
	Music song;

	@Override
	public void initialize() {
		song = Gdx.audio.newMusic(Gdx.files.internal("assets/music/Megalovania.mp3"));
		song.setLooping(true);
		song.setVolume(0.3f);
		//song.play();
		animacion=new ArrayList<>();
		animacion=meterSprite(animacion);
		
		button=new Texture("assets/personaje/personaje0.png");
		resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		BaseActor fondo=new BaseActor(0, 0, mainStage);
		fondo.loadTexture("assets/maps/game_bg_05_001.png");
		fondo.setSize(800, 640);
		
		crearSkins();
		alternativa=new Skin(Gdx.files.internal("assets/craftacular/skin/craftacular-ui.json"));
		Label titulo = new Label("GEOMETRY DASH", alternativa);
		uiTable.add(titulo).center().width(250).height(75).pad(5);
		BaseActor background = new BaseActor(0, 0 , mainStage);
		//hace que escuche el stague
		mainStage.addActor(background);
		TextButton button1 = new TextButton("nivel 1", skin);
		button1.addListener(new ClickListener(){
			  public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				 song.stop();
				  Geometry.setActiveScreen(new Nivel1());
	            }
		});
	
		
	
		uiTable.row();
		uiTable.add(button1).center().width(250).height(75).pad(5);
		TextButton btn2 = new TextButton("nivel 2", skin);
		
		btn2.addListener(new ClickListener(){
			  public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
					 song.stop();
					  Geometry.setActiveScreen(new Nivel2());
	            }
		});
		
		uiTable.row();
		uiTable.add(btn2).center().width(250).height(75).pad(5);
		TextButton btn3 = new TextButton("nivel 3", skin);
		uiTable.row();
	
		uiTable.add(btn3).center().width(250).height(75).pad(5);
		btn3.addListener(new ClickListener(){
			  public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
					 song.stop();
	                Geometry.setActiveScreen(new Nivel3());
	            }
		});
		TextButton btnsalir = new TextButton("salir", skin);
	
		btnsalir.addListener(new ClickListener() {
			  @Override
	            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
	                Gdx.app.exit();
	            }
	        });
		uiTable.row();
		uiTable.add(btnsalir).center().width(250).height(100).pad(5);
		uiTable.row();
		mainStage.addActor(uiTable);
		
	//	uiStage.addActor(uiTable);

		
	}

	private ArrayList<Sprite> meterSprite(ArrayList<Sprite> animacion2) {
		
	
		return animacion2;
	}
	private void crearSkins() {
		//creando fuente
		BitmapFont font=new BitmapFont();
		skin=new Skin();
		skin.add("default", font);
		//creando textura
		Pixmap pixmap=new Pixmap(Gdx.graphics.getWidth()/4, Gdx.graphics.getHeight()/10, Pixmap.Format.RGB888);
		pixmap.setColor(Color.WHITE);
		pixmap.fill();
		skin.add("background", new Texture(pixmap));
		//estilo del boton
		TextButton.TextButtonStyle estilo=new TextButton.TextButtonStyle();
		estilo.up=skin.newDrawable("background",Color.GRAY);
		estilo.down=skin.newDrawable("background",Color.DARK_GRAY);
		estilo.checked=skin.newDrawable("background",Color.DARK_GRAY);
		estilo.over=skin.newDrawable("background",Color.BLUE);
		estilo.font=skin.getFont("default");
		skin.add("default", estilo);
		
		prota=new Actorcubo(button);
		
		mainStage.addActor(prota);

		mainStage.addActor(uiTable);
		
		prota.setPosition(100, 200);
		
	}
	@Override
	public void render(float dt) {
		// TODO Auto-generated method stub
		super.render(dt);
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl20.glClearColor(GL20.GL_DEPTH_BUFFER_BIT, 0, 0, 0);
	}

	@Override
	public void update(float dt) {
	
		

		
	}
	@Override
	public void hide() {
		// TODO Auto-generated method stub
		super.hide();
	
	}
	@Override
	public void dispose() {
		super.dispose();
		mainStage.dispose();
	}

	


}
