import com.badlogic.gdx.Game;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.mygdx.game.Geometry;


public class Launcher
{
    public static void main (String[] args)
    {
        Game myGame = new Geometry(); 
        LwjglApplication launcher = new LwjglApplication( myGame, "Geometry dash", 800, 640 );
    }
}